const express = require('express')
const utils = require('./utils.js')
const app = express()
const port = 3000

app.get('/', (req, res) => {
    res.send(utils.takeShortestWord('hi', 'world'))
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})