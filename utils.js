module.exports.combineWords = (inputA, inputB) => {
    return inputA + inputB;
}

module.exports.takeShortestWord = (inputA, inputB) => {
    return inputA.length >= inputB.length ? inputB : inputA;
}